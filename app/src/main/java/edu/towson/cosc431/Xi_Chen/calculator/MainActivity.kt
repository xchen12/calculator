//source: codeAndroid https://www.youtube.com/watch?v=EpP6KgJtHTk
package edu.towson.cosc431.Xi_Chen.calculator

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*
import net.objecthunter.exp4j.ExpressionBuilder
import java.lang.Exception

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        button0.setOnClickListener { displayInput("0", true) }
        button1.setOnClickListener { displayInput("1", true)}
        button2.setOnClickListener { displayInput("2", true)}
        button3.setOnClickListener { displayInput("3", true)}
        button4.setOnClickListener { displayInput("4", true) }
        button5.setOnClickListener { displayInput("5", true) }
        button6.setOnClickListener { displayInput("6", true) }
        button7.setOnClickListener { displayInput("7", true)}
        button8.setOnClickListener { displayInput("8", true) }
        button9.setOnClickListener { displayInput("9", true) }
        buttonDot.setOnClickListener { displayInput(".", true) }

        buttonAdd.setOnClickListener { displayInput("+", false)}
        buttonSub.setOnClickListener { displayInput("-",  false) }
        buttonMly.setOnClickListener { displayInput("*", false)}
        buttonDiv.setOnClickListener { displayInput("/",  false)}

        buttonCl.setOnClickListener {
            operView.text = ""
            resultView.text = ""
        }

        buttonEql.setOnClickListener {
            try {
                val exp = ExpressionBuilder(operView.text.toString()).build()
                val result = exp.evaluate()
                val result2 = result.toLong()
                if(result == result2.toDouble())
                {
                    resultView.text = result2.toString()
                }else
                {
                    resultView.text = result.toString()
                }

            }catch (e:Exception){
                resultView.text = "Error"
            }

        }

    }

   private fun displayInput(string:String, isempty:Boolean){
        if (resultView.text.isNotEmpty()){
            operView.text = ""
        }

        if (isempty)
        {
            resultView.text = ""
            operView.append(string)
        }
        else{
            operView.append(resultView.text)
            operView.append(string)
            resultView.text = ""
        }
    }


}
